#!pip install langchain
#!pip install chromadb

from langchain.document_loaders import DirectoryLoader
import os
from langchain.embeddings.openai import OpenAIEmbeddings
from langchain.chains import RetrievalQA
from langchain.indexes import VectorstoreIndexCreator
from langchain.text_splitter import CharacterTextSplitter
from langchain.embeddings import OpenAIEmbeddings
from langchain.vectorstores import Chroma
import openai
import pandas as pd
from langchain import OpenAI

openai.api_key = "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"

with open("./input.txt", "r", encoding="utf-8") as f:
    text = f.read()

texts = CharacterTextSplitter(chunk_size=800, chunk_overlap=200).split_text(text)

embeddings = OpenAIEmbeddings(openai_api_key=openai.api_key)
database = Chroma.from_texts(texts, embeddings)
retriever = database.as_retriever(search_type="similarity", search_kwargs={"k": 20})
qa = RetrievalQA.from_chain_type(
    llm=OpenAI(openai_api_key=openai.api_key, temperature=0),
    chain_type="stuff",
    retriever=retriever,
    return_source_documents=True,
)
# query = "Describe this tender in 1 short sentence"
# result = qa({"query": query})
# print(result["result"] + "\n\n\n\n")
# print(result["related"] + "\n\n\n\n")

queries = pd.Series(
    [
        "summarize this text",
        # "Return the minimum requirements to bid for this tender as a list of bullet points",
        # "What is the Currency for this tender? (answer N/A if not mentioned) ",
        # "What are the Terms of Payment? (answer N/A if not mentioned)",
        # "Partial bid allowed? (answer N/A if not mentioned)",
        # "What is the Bid validity for this tender ? (answer N/A if not mentioned)",
        # "What is the Expected time for delivery of service",
    ]
)

full_results = queries.apply(lambda x: qa({"query": x}))
full_results = pd.DataFrame(list(full_results))

for doc in full_results["source_documents"][0]:
    print("_________")
    print(doc.page_content)

print(full_results["result"])

# full_results.to_excel("full_queries.xlsx")
# partial_queries = pd.Series(
#     [
#         "Describe this tender in 1 short sentence",
#         "Return the minimum requirements to bid for this tender as a list of bullet points",
#         "What is the Currency for this tender? ",
#         "What are the Terms of Payment?",
#         "Partial bid allowed?",
#         "What is the Bid validity for this tender ?",
#         "What is the Expected time for delivery of service / products?",
#         "Had answers been given to questions or queries to the tender by the UN?",
#         "Is there a tour",
#     ]
# )

# query = "Return the minimum requirements to bid for this tender as a list of bullet points"
# result = qa({"query": query})
# print(result.get('result') + "\n\n\n\n")

# query = "What is the Currency for this tender? (answer N/A if not mentioned) "
# result = qa({"query": query})
# print(result.get('result') + "\n\n\n\n")

# query = "What are the Terms of Payment? (answer N/A if not mentioned)"
# result = qa({"query": query})
# print(result.get('result') + "\n\n\n\n")

# query = "Partial bid allowed? (answer N/A if not mentioned)"
# result = qa({"query": query})
# print(result.get('result') + "\n\n\n\n")

# query = "What is the Bid validity for this tender ? (answer N/A if not mentioned)"
# result = qa({"query": query})
# print(result.get('result') + "\n\n\n\n")

# query = "What is the Expected time for delivery of service / products? (answer N/A if not mentioned)"
# result = qa({"query": query})
# print(result.get('result') + "\n\n\n\n")

# query = "Had answers been given to questions or queries to the tender by the UN? (answer N/A if not mentioned)"
# result = qa({"query": query})
# print(result.get('result') + "\n\n\n\n")

# query = "Is there a tour / meeting contractors in the field and is it mandatory? (answer N/A if not mentioned)"
# result = qa({"query": query})
# print(result.get('result') + "\n\n\n\n")

# query = "What is the Last time to post questions? (answer N/A if not mentioned)"
# result = qa({"query": query})
# print(result.get('result') + "\n\n\n\n")

# query = "Is a letter of acknowledgement required (date of submission)?(answer N/A if not mentioned)"
# result = qa({"query": query})
# print(result.get('result') + "\n\n\n\n")

# query = "Return a list of categories of items and exact quantities of said category in this tender (unless this tender is about a certain activity that needs to be executed like construction for example then specify about the activities that need to be carried out or the scope of work as a list of specific activities)"
# result = qa({"query": query})
# print(result.get('result') + "\n\n\n\n")

# query = "What is the Required Location for delivery of service / products? (answer N/A if not mentioned)"
# result = qa({"query": query})
# print(result.get('result') + "\n\n\n\n")
