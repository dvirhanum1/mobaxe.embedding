from collections import deque
from html.parser import HTMLParser
from urllib.parse import urlparse
import pandas as pd
import tiktoken
import openai
import numpy as np
from openai.embeddings_utils import distances_from_embeddings, cosine_similarity
from typing import List
from io import StringIO
import logging


def set_api_key(key: str):
    openai.api_key = key


def create_file_embeddings(text: str, max_tokens=120) -> str:
    logging.debug("removing new lines...", text)

    clean_text = _remove_newlines(text)

    logging.debug("finished removing new lines.", text)
    logging.debug("tokenzing...", text)

    df = pd.DataFrame([clean_text], columns=["text"])
    df = _tokenize(df, max_tokens)

    logging.debug("finished tokenzing.", text)
    logging.debug("creaating embedding file...", text)

    text = _create_embedding(df)

    logging.debug("finished creating embedding file.", text)

    return text


def answer_questions(embedding: str, questions: List[str], context: str):
    logging.debug('here')
    responses: List[str] = []
    df = pd.DataFrame([], columns=["fname", "text"])
    textIO = StringIO(embedding)
    logging.debug('still here: %s', textIO)
    df = pd.read_csv(textIO, index_col=0)
    logging.debug('yeah')
    df["embeddings"] = df["embeddings"].apply(eval).apply(np.array)

    logging.debug("going through the following questions: %s", questions)

    for q in questions:
        logging.debug("question 1: %s", q)
        responses.append(answer_question(df, question=q, context=context))

    return responses


def _remove_newlines(text: str) -> str:
    text = text.replace("\n", " ")
    text = text.replace("\\n", " ")
    text = text.replace("  ", " ")
    text = text.replace("  ", " ")
    return text


def _tokenize(df: pd.DataFrame, max_tokens: int) -> pd.DataFrame:
    # Load the cl100k_base tokenizer which is designed to work with the ada-002 model
    tokenizer = tiktoken.get_encoding("cl100k_base")

    # Tokenize the text and save the number of tokens to a new column
    df["n_tokens"] = df.text.apply(lambda x: len(tokenizer.encode(x)))

    # Function to split the text into chunks of a maximum number of tokens
    def _split_into_many(text, max_tokens):
        # Split the text into sentences
        sentences = text.split(". ")

        # Get the number of tokens for each sentence
        n_tokens = [len(tokenizer.encode(" " + sentence)) for sentence in sentences]

        chunks = []
        tokens_so_far = 0
        chunk = []

        # Loop through the sentences and tokens joined together in a tuple
        for sentence, token in zip(sentences, n_tokens):
            # If the number of tokens so far plus the number of tokens in the current sentence is greater
            # than the max number of tokens, then add the chunk to the list of chunks and reset
            # the chunk and tokens so far
            if tokens_so_far + token > max_tokens:
                chunks.append(". ".join(chunk) + ".")
                chunk = []
                tokens_so_far = 0

            # If the number of tokens in the current sentence is greater than the max number of
            # tokens, go to the next sentence
            if token > max_tokens:
                continue

            # Otherwise, add the sentence to the chunk and add the number of tokens to the total
            chunk.append(sentence)
            tokens_so_far += token + 1

        # Add the last chunk to the list of chunks
        if chunk:
            chunks.append(". ".join(chunk) + ".")

        return chunks

    shortened = []

    # Loop through the dataframe
    for row in df.iterrows():
        # If the text is None, go to the next row
        if row[1]["text"] is None:
            continue

        # If the number of tokens is greater than the max number of tokens, split the text into chunks
        if row[1]["n_tokens"] > max_tokens:
            shortened += _split_into_many(row[1]["text"], max_tokens)

        # Otherwise, add the text to the list of shortened texts
        else:
            shortened.append(row[1]["text"])

    df = pd.DataFrame(shortened, columns=["text"])
    df["n_tokens"] = df.text.apply(lambda x: len(tokenizer.encode(x)))

    return df


def _create_embedding(df: pd.DataFrame) -> str:
    df["embeddings"] = df.text.apply(
        lambda x: openai.Embedding.create(input=x, engine="text-embedding-ada-002")[
            "data"
        ][0]["embedding"]
    )
    return df.to_csv()


def _create_context(question, df, max_len=1800, size="ada", num_documents=4):
    """
    Create a context for a question by finding the most similar context from the dataframe
    """

    # Get the embeddings for the question
    q_embeddings = openai.Embedding.create(
        input=question, engine="text-embedding-ada-002"
    )["data"][0]["embedding"]

    # Get the distances from the embeddings
    df["distances"] = distances_from_embeddings(
        q_embeddings, df["embeddings"].values, distance_metric="cosine"
    )

    returns = []
    cur_len = 0

    # Sort by distance and add the text to the context until the context is too long
    for i, row in df.sort_values("distances", ascending=True).iterrows():
        # Add the length of the text to the current length
        cur_len += row["n_tokens"] + num_documents

        # If the context is too long, break
        if cur_len > max_len:
            break

        # Else add it to the text that is being returned
        returns.append(row["text"])

    # Return the context
    return "\n\n###\n\n".join(returns)


def answer_question(
    df,
    model="text-davinci-003",
    question="",
    max_len=1800,
    size="ada",
    debug=False,
    max_tokens=150,
    stop_sequence=None,
    context="",
) -> str:
    """
    Answer a question based on the most similar context from the dataframe texts
    """
    _context = _create_context(
        question,
        df,
        max_len=max_len,
        size=size,
    )
    # If debug, print the raw model response
    if debug:
        print("Context:\n" + _context)
        print("\n\n")

    try:
        # Create a completions using the questin and context
        prompt = context.format(_context, question)
        response = openai.Completion.create(
            prompt=prompt,
            temperature=0,
            max_tokens=max_tokens,
            top_p=1,
            frequency_penalty=0,
            presence_penalty=0,
            stop=stop_sequence,
            model=model,
        )
        return response["choices"][0]["text"].strip()
    except Exception as e:
        print(e)
        return ""
