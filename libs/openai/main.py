import argparse
from api import create_file_embeddings, answer_questions, set_api_key
from dotenv import load_dotenv
import logging
import json


try:
    SEPARATOR = "#finish#"

    # Create the parser
    parser = argparse.ArgumentParser(
        description="Mobaxe Emebding program for finding answers on a spesific context"
    )

    # Add arguments
    parser.add_argument(
        "--debug",
        type=bool,
        action=argparse.BooleanOptionalAction,
        default=False,
        help="Enable debug log",
        required=False,
    )
    parser.add_argument(
        "--log-file-name",
        type=str,
        help="log file name",
        default="python-log",
        required=False,
    )
    parser.add_argument(
        "--input",
        metavar="filename",
        type=str,
        help="The path for the input file",
        required=True,
    )
    parser.add_argument(
        "--output",
        metavar="filename",
        type=str,
        help="The path for the output file",
        required=False,
    )
    parser.add_argument(
        "--embedding",
        type=bool,
        action=argparse.BooleanOptionalAction,
        help="Return embeddings",
    )
    parser.add_argument(
        "--q", nargs="+", type=str, help="A list of questions", required=False
    )
    parser.add_argument(
        "--c",
        type=str,
        help="The context that mml model will use",
        default="Answer the question based on the context below, and if the question can't be answered based on the context, say \"I don't know\"\n\nContext: {}\n\n---\n\nQuestion: {}\nAnswer:",
        required=False,
    )
    parser.add_argument(
        "--env", type=str, help="your .env file variable", required=False
    )

    parser.add_argument("--api-key", type=str, help="openai api key", required=True)

    # Parse the arguments
    args = parser.parse_args()

    logging.basicConfig(filename=args.log_file_name + ".log", level=logging.DEBUG)
    logging.basicConfig(filename=args.log_file_name + ".error.log", level=logging.ERROR)

    load_dotenv(dotenv_path=args.env or ".env")
    set_api_key(args.api_key)

    ## Validation
    if args.embedding is None:
        parser.error("--embedding or --no-embedding is required.")

    if args.embedding is False and args.q is None:
        parser.error("--q is required.")

    with open(args.input, "r", encoding="utf-8") as f:
        text = f.read()

        if args.embedding:
            with open(args.output, "w", encoding="utf-8") as f:
                embeddings = create_file_embeddings(text=text)
                f.write(embeddings)
        else:
            answer = answer_questions(embedding=text, questions=args.q, context=args.c)
            print(json.dumps(answer))
except Exception as e:
    # This block will catch all exceptions derived from the base Exception class.
    logging.debug(e)
