# Pull base image
FROM python:3.9-slim

# Set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV ENVIRONMENT=production

ENV LISTEN_PORT=5000
EXPOSE 5000

WORKDIR /app
COPY . .
RUN pip install -r ./requirements.txt \
    && pip install -r ./requirements.prod.txt

ENTRYPOINT ["gunicorn", "app:app", "--workers", "4"]

