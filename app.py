from flask import Flask, jsonify, request
from libs.openai.api import answer_questions, create_file_embeddings, set_api_key
import os
from dotenv import load_dotenv
import logging
import tempfile
import uuid

logging.basicConfig(filename="debug.log", level=logging.DEBUG)

app = Flask(__name__)

load_dotenv()
set_api_key(os.getenv("OPENAI_API_KEY"))

temp_dir = tempfile.gettempdir()


@app.route("/api/embedding/create", methods=["POST"])
def create_embedding():
    text = request.data.decode("utf-8")  # Decode the request body as plain text
    text = create_file_embeddings(text)
    filename = str(uuid.uuid4())
    extension = ".txt"
    fullPath = temp_dir + "/" + filename + extension
    logging.debug("temp file is %s", fullPath)
    with open(fullPath, "w") as f:
        f.write(text)
    return jsonify({"filename": filename})


@app.route("/api/embedding/ask", methods=["POST"])
def ask_embedding():
    CONTEXT = "Answer the question based on the context below, and if the question can't be answered based on the context, say \"I don't know\"\n\nContext: {}\n\n---\n\nQuestion: {}\nAnswer:"
    data = request.get_json()
    context = data.get("context")
    questions = data.get("questions")

    with open(temp_dir + "/" + data.get("filename") + ".txt", "r") as f:
        text = f.read()
        answers = answer_questions(
            text, questions, context if context is not None else CONTEXT
        )
    return jsonify({"questions": questions, "answers": answers})


@app.route("/api", methods=["GET"])
def get_data():
    data = {"message": "Hello, World!"}
    return jsonify(data)


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000)
